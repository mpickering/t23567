{-# LANGUAGE Arrows #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE DerivingStrategies #-}
{-# OPTIONS_GHC -funfolding-use-threshold=640 -fmax-simplifier-iterations=2  #-}

module Cache ()

where

import RunCacheBuild
import Control.Monad.Reader
import Control.Monad.State


newtype CacheRWT m a
  = CacheRWT (ReaderT () (StateT () m) a)
  deriving newtype
    ( Functor,
      Applicative,
      Monad
    )

instance
  ( MonadIO m
  ) =>
  CacheRWM2 (CacheRWT m)
  where
  tryBuildSchemaCacheWithOptions = CacheRWT $ do
    result <-
      runCacheBuildM $ flip runReaderT undefined $ undefined
    undefined

class CacheRWM2 m where
  tryBuildSchemaCacheWithOptions :: m a






