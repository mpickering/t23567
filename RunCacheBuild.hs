{-# LANGUAGE Arrows #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE NoStrictData #-}
{-# LANGUAGE DerivingStrategies #-}
{-# OPTIONS_GHC -funfolding-use-threshold=640 -fmax-simplifier-iterations=2 #-}

module RunCacheBuild
  (
    runCacheBuild,
    runCacheBuildM
  )
where

import Prelude
import Control.Monad.Except
import Control.Monad.Reader


data CacheBuildParams = CacheBuildParams
  { _cbpManager :: ()
  }

-- | The monad in which @'RebuildableSchemaCache' is being run
newtype CacheBuild a = CacheBuild (ReaderT CacheBuildParams (ExceptT () IO) a)
  deriving newtype
    ( Functor,
      Applicative,
      Monad
    )

runCacheBuild ::
  ( MonadIO m
  ) =>
  CacheBuildParams -> CacheBuild a -> m a
runCacheBuild params (CacheBuild m) = undefined

runCacheBuildM ::
  ( MonadIO m
  ) =>
  CacheBuild a ->
  m a
runCacheBuildM m = do
  params <-
    CacheBuildParams
      <$> pure ()
  runCacheBuild params m

{-# NOINLINE runCacheBuild #-}
