```
[nix-shell:~/graphql-engine/repro3]$ ~/ghc-9.6/_perf/stage1/bin/ghc Cache.hs  -O2 -fforce-recomp -dno-debug-output
[1 of 2] Compiling RunCacheBuild    ( RunCacheBuild.hs, RunCacheBuild.o )
[2 of 2] Compiling Cache            ( Cache.hs, Cache.o )

<no location info>: error:
    panic! (the 'impossible' happened)
  GHC version 9.6.0.20230309:
	lookupIdSubst
  $dMonad_s22W
  InScope {$cp1Applicative_a1Pd $cpure_a1Pf $c>>_a1TE $creturn_a1Um
           m_a1UX $dMonadIO_a1UY $ctryBuildSchemaCacheWithOptions_a1V0 a_a1V3
           $krep_a1XF $krep_a1XG $krep_a1XH $krep_a1XI $krep_a1XJ $krep_a1XK
           $krep_a1XL $krep_a1XM $krep_a1XN $krep_a1XO $krep_a1XP $tc'CacheRWT
           $tcCacheRWT $tcCacheRWM2 $fCacheRWM2TYPECacheRWT $fFunctorCacheRWT
           $fApplicativeCacheRWT $fMonadCacheRWT $trModule $c>>=_s21H
           $c<*_s21I $c*>_s21J $cliftA2_s21K $c<*>_s21L $c<$_s21P $cfmap_s21Q
           $trModule_s21R $trModule_s21S $trModule_s21T $trModule_s21U
           $krep_s21V $krep_s21W $krep_s21X $krep_s21Y $krep_s21Z $krep_s220
           $krep_s221 $krep_s222 $tcCacheRWT_s223 $tcCacheRWT_s224 $krep_s225
           $krep_s226 $tc'CacheRWT_s227 $tc'CacheRWT_s228 $tcCacheRWM2_s229
           $tcCacheRWM2_s22a $creturn_s22m $c>>_s22n $cpure_s22o loc_s22q
           loc_s22s loc_s22u loc_s22w loc_s22y loc_s22A loc_s22C $dIP_s22E
           $dIP_s22G $dIP_s22I $dMonad_s22K $dMonadIO_s22M $dMonadIO_s22O
           eta_s22Q ww1_s22S ww1_s22U ww4_s22Y ww5_s232}
  Call stack:
      CallStack (from HasCallStack):
        callStackDoc, called at compiler/GHC/Utils/Panic.hs:189:37 in ghc:GHC.Utils.Panic
        pprPanic, called at compiler/GHC/Core/Subst.hs:197:17 in ghc:GHC.Core.Subst
        lookupIdSubst, called at compiler/GHC/Core/SimpleOpt.hs:252:10 in ghc:GHC.Core.SimpleOpt
        simple_opt_expr, called at compiler/GHC/Core/SimpleOpt.hs:236:5 in ghc:GHC.Core.SimpleOpt
        simple_opt_clo, called at compiler/GHC/Core/SimpleOpt.hs:414:31 in ghc:GHC.Core.SimpleOpt
        finish_app, called at compiler/GHC/Core/SimpleOpt.hs:345:5 in ghc:GHC.Core.SimpleOpt
        simple_app, called at compiler/GHC/Core/SimpleOpt.hs:348:5 in ghc:GHC.Core.SimpleOpt
        simple_app, called at compiler/GHC/Core/SimpleOpt.hs:254:27 in ghc:GHC.Core.SimpleOpt
        simple_opt_expr, called at compiler/GHC/Core/SimpleOpt.hs:236:5 in ghc:GHC.Core.SimpleOpt
        simple_opt_clo, called at compiler/GHC/Core/SimpleOpt.hs:414:31 in ghc:GHC.Core.SimpleOpt
        finish_app, called at compiler/GHC/Core/SimpleOpt.hs:345:5 in ghc:GHC.Core.SimpleOpt
        simple_app, called at compiler/GHC/Core/SimpleOpt.hs:348:5 in ghc:GHC.Core.SimpleOpt
        simple_app, called at compiler/GHC/Core/SimpleOpt.hs:254:27 in ghc:GHC.Core.SimpleOpt
        simple_opt_expr, called at compiler/GHC/Core/SimpleOpt.hs:236:5 in ghc:GHC.Core.SimpleOpt
        simple_opt_clo, called at compiler/GHC/Core/SimpleOpt.hs:414:31 in ghc:GHC.Core.SimpleOpt
        finish_app, called at compiler/GHC/Core/SimpleOpt.hs:345:5 in ghc:GHC.Core.SimpleOpt
        simple_app, called at compiler/GHC/Core/SimpleOpt.hs:348:5 in ghc:GHC.Core.SimpleOpt
        simple_app, called at compiler/GHC/Core/SimpleOpt.hs:348:5 in ghc:GHC.Core.SimpleOpt
        simple_app, called at compiler/GHC/Core/SimpleOpt.hs:254:27 in ghc:GHC.Core.SimpleOpt
        simple_opt_expr, called at compiler/GHC/Core/SimpleOpt.hs:148:5 in ghc:GHC.Core.SimpleOpt
        simpleOptExprWith, called at compiler/GHC/Core/Opt/Specialise.hs:1254:11 in ghc:GHC.Core.Opt.Specialise
  CallStack (from HasCallStack):
    panic, called at compiler/GHC/Utils/Error.hs:454:29 in ghc:GHC.Utils.Error


Please report this as a GHC bug:  https://www.haskell.org/ghc/reportabug
```
